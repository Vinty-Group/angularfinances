import {Component, inject} from '@angular/core';
import {AuthService} from "../../../../services/auth.service";

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css']
})
export class AdminLayoutComponent {

    authService = inject(AuthService);
    logout(event: Event) {
        this.authService.logout();
        console.log('logout')
    }
}
