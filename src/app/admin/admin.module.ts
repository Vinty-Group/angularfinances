import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {AdminLayoutComponent} from './shared/components/admin-layout/admin-layout.component';
import {LoginPageComponent} from './components/login-page/login-page.component';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
    declarations: [
        AdminLayoutComponent,
        LoginPageComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '', component: AdminLayoutComponent, children: [
                    {path: '', pathMatch: 'full', component: LoginPageComponent},
                ]
            }
        ]),
        ReactiveFormsModule,
        HttpClientModule
    ]
})
export class AdminModule {
}
