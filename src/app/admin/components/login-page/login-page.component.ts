import {Component, inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {IUser} from "../../../shared/interfaces";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";
import {finalize} from "rxjs";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit{
  form: FormGroup;
  fb = inject(FormBuilder);
  submitted = false;
  message: string;

  router: Router = inject(Router);
  rout: ActivatedRoute = inject(ActivatedRoute);
  authService: AuthService = inject(AuthService);

  ngOnInit(): void {
    this.rout.queryParams.subscribe((params: Params) => {
      if (params["loginAgain"]) {
        this.message = "Пожалуйста введите данные.";
      } else if (params["authFailed"]) {
        this.message = "Сессия истекла. Введите данные заново.";
      }
    });

    this.form = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null,Validators.required]
    })
  }

  submit() {
    if (this.form.invalid){
      return;
    }
    this.submitted = true;
    const user: IUser = {
      email: this.form.value.email,
      password: this.form.value.password
    };

    this.authService.login(user)
        .pipe(finalize(() => this.submitted = false))
        .subscribe(() => {
          this.router.navigate(['/']).then();
        });

    this.submitted = false;
  }
}
