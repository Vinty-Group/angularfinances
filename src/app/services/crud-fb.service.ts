import {inject, Injectable} from '@angular/core';
import {IExpense} from "../shared/interfaces";
import {map, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class CrudFBService {

    http = inject(HttpClient);

    create(expense: IExpense): Observable<{name: string}> {
        return this.http.post<{name: string}>(`${environment.fbDbUrl}/expenses.json`, expense);
    }

    update(expense: IExpense): Observable<{name: string}> {
        const {name, sum, currency} = expense;
        return this.http.patch<{name: string}>(`${environment.fbDbUrl}/expenses/${expense.id}.json`, {name, sum, currency});
    }

    getExpanses() {
        return this.http.get<Record<string, IExpense>>(`${environment.fbDbUrl}/expenses.json`)
            .pipe(map((response: Record<string, IExpense>) =>
                Object.keys(response)
                    .map(key => ({...response[key], id: key}))
            ));
    }

    getExpanse(id) {
        return this.http.get<IExpense>(`${environment.fbDbUrl}/expenses/${id}.json`)
    }

    remove(id: string): Observable<any> {
        return this.http.delete<any>(`${environment.fbDbUrl}/expenses/${id}.json`);
    }
}
