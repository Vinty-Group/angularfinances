export interface IExpense {
    id?: string,
    name: string,
    sum: number,
    currency: string,
    total?: number
}

export interface IUser {
    email: string,
    password: string,
    returnSecureToken?: boolean
}

export interface IFbAuthResponse {
    idToken: string,
    expiresIn: string
}

export interface IEnvironment {
    production: boolean,
    apiKey: string,
    fbDbUrl: string
}

export interface FbResponse {
    name: string
}
