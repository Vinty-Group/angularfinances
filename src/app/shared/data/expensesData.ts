import {IExpense} from "../interfaces";

export const expenses: IExpense[] = [
    {
        "id": '1',
        "name": 'На ребенка',
        "sum": 300,
        "currency": 'USD'
    },
    {
        "id": '2',
        "name": 'Ссуда ЛВО',
        "sum": 730,
        "currency": 'BYN'
    },
    {
        "id": '3',
        "name": 'Кредит - квартира ',
        "sum": 500,
        "currency": 'BYN'
    },
    {
        "id": '4',
        "name": 'Кредит - машина',
        "sum": 500,
        "currency": 'BYN'
    },
    {
        "id": '5',
        "name": 'Интернет Пирс',
        "sum": 40,
        "currency": 'BYN'
    },
    {
        "id": '6',
        "name": 'Коммунальные - Пирс',
        "sum": 100,
        "currency": 'BYN'
    },
    {
        "id": '7',
        "name": 'Телефоны',
        "sum": 50,
        "currency": 'BYN'
    },
    {
        "id": '8',
        "name": 'Прочее...',
        "sum": 200,
        "currency": 'BYN'
    }
]
