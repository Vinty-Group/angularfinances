import {Component, inject} from '@angular/core';
import {AuthService} from "../../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.css']
})
export class MainLayoutComponent {

    authService = inject(AuthService);
    router = inject(Router);

    logout(event: Event) {
        this.authService.logout();
        this.router.navigate(['/admin']);
    }
}
