import {Component, inject} from '@angular/core';
import {ModalService} from "../../services/modal.service";

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent {

  modalService: ModalService = inject(ModalService);
  closeModal() {
    this.modalService.closeModal();
  }
}
