import {Component, inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";

import {IExpense} from "../../shared/interfaces";
import {AlertService} from "../../shared/alert.service";
import {ModalService} from "../../services/modal.service";
import {Subscription} from "rxjs";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {CrudFBService} from "../../services/crud-fb.service";

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit, OnDestroy {
    // FORMS
    form: FormGroup;
    formModal: FormGroup;

    // D-INJECT
    fb = inject(FormBuilder);
    alertService = inject(AlertService);
    modalService = inject(ModalService);
    authService = inject(AuthService);
    crudFBService = inject(CrudFBService);
    router = inject(Router);

    // VAR
    expensesData: IExpense[] = [];
    submitted = false;
    cursUSD = 3.00;
    cursEUR = 3.56;
    usdSumInput = 2150;
    totalAmountBYB = 0;
    sumOfAllPayment = 0;
    remainder = 0;
    mSub: Subscription = new Subscription();
    currencies = [
        'BYN',
        'USD',
        'EUR'
    ];

    ngOnInit(): void {
        if (!this.authService.isAuthenticated()) {
            return this.goAway();
        }
        this.getExpanses();

        this.form = this.fb.group({
            sumUSD: [this.usdSumInput],
            curs: [this.cursUSD],
        });

        this.form.valueChanges.subscribe(value => {
            this.reCalculate();
        })
    }

    goAway() {
        this.router.navigate(['/admin']).then();
    }

    reCalculate() {
        if (this.form.invalid) {
            return;
        }
        this.usdSumInput = this.form.value.sumUSD;
        this.cursUSD = this.form.value.curs;
        this.calculateAll();
    }

    calculateAll() {
        this.expensesData = this.expensesData
            .map(expense => ({...expense, total: this.currencyTransfer(expense)}))
        // общая сумма полученная в рублях
        this.totalAmountBYB = +(this.usdSumInput * +this.cursUSD).toFixed(2);
        // суммируем расходы
        this.sumOfAllPayment = this.expensesData?.length ? this.calculateExpenses() : 0;
        // от основной суммы отнимаем расходы
        this.remainder = this.totalAmountBYB - this.sumOfAllPayment;
    }

    calculateExpenses(): number {
        return this.expensesData
            .map(expense => expense.total)
            .reduce((prev, curr) => +prev + +curr);
    }

    currencyTransfer(expense: IExpense): number {
        const calc = {
            USD: +(expense?.sum * +this.cursUSD).toFixed(2),
            EUR: +(expense?.sum * +this.cursEUR).toFixed(2),
        }
        // есть ли
        return calc[expense?.currency] === 0 ? '' : calc[expense?.currency] || expense?.sum;
    }

    submit() {
        this.reCalculate();
    }

    onClickPlus() {
        this.editExpense();
        this.modalService.openModal();
    }

    addExpenseSubmit() {
        const val = this.formModal.value;
        const expense: IExpense = {
            name: val?.nameModal,
            currency: val?.currencyModal,
            sum: val?.sumModal,
            id: val?.id
        }
        return !val?.id ? this.saveExpense(expense) : this.updateExpense(expense);
    }

    removeExpense(expense: IExpense) {
        this.crudFBService.remove(expense.id).subscribe();
        this.expensesData = this.expensesData.filter(p => p !== expense);
        this.reCalculate();
    }

    editExpense(expense?: IExpense) {
        this.formModal = this.fb.group({
            id: [expense?.id],
            nameModal: [expense?.name || ''],
            currencyModal: [expense?.currency || 'BYN'],
            sumModal: [expense?.sum || '']
        });
        this.modalService.openModal();
    }

    saveExpense(expense: IExpense) {
        return this.crudFBService.create(expense).subscribe(({name}) => {
            this.expensesData = [...this.expensesData, {...expense, id: name}];
            this.reCalculate();
            this.modalService.closeModal();
            this.alertService.success('Статьи расходов изменены.');
        });
    }

    updateExpense(expense: IExpense) {
        return this.crudFBService.update(expense).subscribe(({name}) => {
            this.expensesData = this.expensesData.map(m => m.id !== expense.id ? m : expense)
            // let index = this.expensesData.indexOf(expense);
            // this.expensesData[index] = expense;

            this.reCalculate();
            this.modalService.closeModal();
            this.alertService.success('Статьи расходов изменены.');
        });
    }

    getExpanses() {
        this.crudFBService.getExpanses().subscribe(expenses => {
            this.expensesData = expenses.map(expense => ({...expense, total: this.currencyTransfer(expense)}));
            this.calculateAll();
        })
    }

    ngOnDestroy(): void {
        this.mSub.unsubscribe();
    }
}


