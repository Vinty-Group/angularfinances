import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IExpense} from "../../shared/interfaces";

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.css']
})
export class ExpensesComponent implements OnInit{
  @Input() expenses: IExpense[];
  @Output() remove: EventEmitter<IExpense> = new EventEmitter<IExpense>();
  @Output() expense: EventEmitter<IExpense> = new EventEmitter<IExpense>();
  ngOnInit(): void {
  }

  removeExpense(expense: IExpense) {
    this.remove.emit(expense);
  }

  rowClick(expense: IExpense) {
    this.expense.emit(expense);
  }
}
